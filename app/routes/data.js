class Company {
    constructor(id, company, contact, country) {
        this.id = id;
        this.company = company;
        this.contact = contact;
        this.country = country;
    }
}

module.exports = Company;