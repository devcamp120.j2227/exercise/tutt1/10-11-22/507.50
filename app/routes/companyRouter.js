const Company = require("./data");
const array = [];

let company_1 = new Company(1, "Alfreds Futterkiste", "Maria Anders", "Germany")
let company_2 = new Company(2, "Centro comercial Moctezuma", "Francisco Chang", "Mexico")
let company_3 = new Company(3, "Ernst Handel", "Roland Mendel", "Australia")
let company_4 = new Company(4, "Island Trading", "Helen Bennett", "UK")
let company_5 = new Company(5, "Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada")
let company_6 = new Company(6, "Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy")

array.push(company_1);
array.push(company_2);
array.push(company_3);
array.push(company_4);
array.push(company_5);
array.push(company_6);

module.exports = array;