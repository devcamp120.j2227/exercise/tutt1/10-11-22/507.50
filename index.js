const express = require("express");
const app = express();
const port = 8000;
const array = require("./app/routes/companyRouter");

app.get("/companies", (req, res) => {
    res.status(200).json({
        array
    });
});

app.listen(port, () => {
    console.log("App listenting on port: ", port);
});